export default function CounterButton(props)
{
    return <button className={StyleSheet.counterBtn}
    onClick={props.onClickHandler}>

        {props.children}
    </button>
}