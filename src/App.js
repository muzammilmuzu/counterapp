import Title from './components/Title';
import { useState } from 'react';
import './App.css';
import styles from './styles.module.css';
import CounterButton from './components/CounterButton';
import Count from './components/Count';


function App() {
    const [count, setcount] = useState(0);
    function incrementCount(){
        setcount(count + 1)
    }
    function decrementCount(){
        setcount(count - 1)
    }

    
    return (
        <div className={styles.body} >
            <div className={styles.counterContainer}>

                <Title title={"Counter App"} />
                <div className={styles.buttonWrapper}>
                    <div className={styles.counterPlusBtn} >
                        <CounterButton onClickHandler={decrementCount}> - </CounterButton>
                    </div>
                    <div className={styles.count}>
                        <Count value={count}></Count>
                    </div>
                    <div className={styles.counterMinusBtn}>
                        <CounterButton onClickHandler={incrementCount}> + </CounterButton>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default App;